<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Sale;

class Customer extends Model
{
    protected $fillable = [
        'name', 'father_lastname', 'mother_lastname', 'rfc'
    ];

    public function sales(){
    	return $this->hasMany(Sale::class);
    }

    public function getFullNameAttribute() {
        return ucfirst($this->name) . ' ' . ucfirst($this->father_lastname) . ' ' . ucfirst($this->mother_lastname);
    }


}