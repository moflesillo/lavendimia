<?php

namespace App\Http\Controllers;

use App\Config;
use Illuminate\Http\Request;

class ConfigController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $configs = Config::all();
        $config = $configs->last();

        if(!empty($config)){

            return view('configs.edit',compact('config'));
        }

        return view('configs.create');
    }

    public function create(){
        return view('configs.create');
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $messages = [
            'financing_rate.numeric' => 'Tasa de Financiamiento debe ser un número.',
            'hooking_percentage.numeric' => '% de Enganche debe ser un número.',
            'deadline.numeric' => 'Plazo Máximo debe ser un número.',
        ];

        $request->validate([
            'financing_rate' => 'numeric',
            'hooking_percentage' => 'numeric',
            'deadline' => 'numeric',
        ], $messages);
  
        Config::create($request->all());
   
        return redirect()->route('configs.index')
                        ->with('success','“Bien Hecho. La configuración ha sido registrada”.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Config  $config
     * @return \Illuminate\Http\Response
     */
    public function show(Config $config)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Config  $config
     * @return \Illuminate\Http\Response
     */
    public function edit(Config $config)
    {
        return view('configs.edit',compact($config));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Config  $config
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Config $config)
    {
         $messages = [
            'financing_rate.numeric' => 'Tasa de Financiamiento debe ser un número.',
            'hooking_percentage.numeric' => '% de Enganche debe ser un número.',
            'deadline.numeric' => 'Plazo Máximo debe ser un número.',
        ];

        $request->validate([
            'financing_rate' => 'numeric',
            'hooking_percentage' => 'numeric',
            'deadline' => 'numeric',
        ], $messages);
  
        $config->update($request->all());
  
        return redirect()->route('configs.index')
                        ->with('success','Bien Hecho. La configuración ha sido registrada.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Config  $config
     * @return \Illuminate\Http\Response
     */
    public function destroy(Config $config)
    {
        //
    }
}
