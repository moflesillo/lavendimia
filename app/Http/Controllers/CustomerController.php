<?php

namespace App\Http\Controllers;

use App\Customer;
use Illuminate\Http\Request;

class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $customers = Customer::latest()->paginate(5);
  
        return view('customers.index',compact('customers'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $customers = Customer::all();
        $customer = $customers->last();

        return view('customers.create',compact('customer'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $messages = [
            'name.required' => 'Nombre es requerido.',
            'father_lastname.required' => 'Apellido Paterno es requerido.',
            'mother_lastname.required' => 'Apellido Materno es requerido.',
            'rfc.required' => 'RFC es requerido.',
            'rfc.unique' => 'Ya existe un cliente con ese RFC',
        ];

        $request->validate([
            'name' => 'required',
            'father_lastname' => 'required',
            'mother_lastname' => 'required',
            'rfc' => ['unique:customers,rfc','required'],
        ], $messages);
  
        Customer::create($request->all());
   
        return redirect()->route('customers.index')
                        ->with('success','“Bien Hecho. El cliente ha sido registrado correctamente”.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function show(Customer $customer)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function edit(Customer $customer)
    {
        return view('customers.edit',compact('customer'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Customer $customer)
    {
        $messages = [
            'name.required' => 'Nombre es requerido.',
            'father_lastname.required' => 'Apellido Paterno es requerido.',
            'mother_lastname.required' => 'Apellido Materno es requerido.',
            'rfc.required' => 'RFC es requerido.',
            'rfc.unique' => 'Ya existe un cliente con ese RFC',
        ];

        $request->validate([
            'name' => 'required',
            'father_lastname' => 'required',
            'mother_lastname' => 'required',
            'rfc' => ['unique:customers,rfc,'.$customer->id,'required'],
        ], $messages);
  
        $customer->update($request->all());
  
        return redirect()->route('customers.index')
                        ->with('success','Se actualizó el Cliente con éxito.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function destroy(Customer $customer)
    {
        //
    }
}
