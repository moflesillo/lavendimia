<?php

namespace App\Http\Controllers;

use App\Article;
use Illuminate\Http\Request;

class ArticleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $articles = Article::latest()->paginate(5);
  
        return view('articles.index',compact('articles'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $articles = Article::all();
        $article = $articles->last();

        return view('articles.create',compact('article'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       $messages = [
            'description.required' => 'Descripción es requerido.',
            'price.required' => 'Precio es requerido.',
            'price.numeric' => 'El precio debe ser un número',
            'existence.required' => 'Existencia es requerido.',
            'existence.numeric' => 'La existencia debe ser un número',
        ];

        $request->validate([
            'description' => 'required',
            'price' => 'required',
            'price' => 'numeric',
            'existence' => 'required',
            'existence' => 'numeric',
        ], $messages);
  
  
        Article::create($request->all());
   
        return redirect()->route('articles.index')
                        ->with('success','“Bien Hecho. El artículo ha sido registrado correctamente”.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function show(Article $article)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function edit(Article $article)
    {
        return view('articles.edit',compact('article'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Article $article)
    {
        $messages = [
            'description.required' => 'Descripción es requerido.',
            'price.required' => 'Precio es requerido.',
            'price.numeric' => 'El precio debe ser un número',
            'existence.required' => 'Existencia es requerido.',
            'existence.numeric' => 'La existencia debe ser un número',
        ];

        $request->validate([
            'description' => 'required',
            'price' => 'required',
            'price' => 'numeric',
            'existence' => 'required',
            'existence' => 'numeric',
        ], $messages);
  
        $article->update($request->all());
  
        return redirect()->route('articles.index')
                        ->with('success','Se actualizó el Artículo con éxito.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function destroy(Article $article)
    {
        //
    }
}
