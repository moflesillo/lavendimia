<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Config extends Model
{
    protected $fillable = [
        'financing_rate', 'hooking_percentage', 'deadline'
    ];
}
