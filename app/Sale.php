<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\Customer;

class Sale extends Model
{
    protected $fillable = [
        'customer_id'
    ];

    public function customer(){
    	return $this->belongsTo(Customer::class);
    }
}
