@extends('layouts.app')
 
@section('content')
    
<div class="container">
    

    

   <div class="row">

       <div class="col-8">
           <h4 class="text-primary" style="position: absolute;
   bottom:0px; top:12px;">Ventas activas</h4>
       </div>
       <div class="col text-right">

           <a href="{{ route('sales.create') }}" class="btn btn-primary btn-lg" style="color:white">
            <i style="font-size:40px; color:#38c172; vertical-align:middle; " class="material-icons">add_circle</i> 
              Nueva Venta</a>
       </div>
       

   </div>
   <hr>
    <table class="table">
      <thead class="thead-light">
        <tr>
          <th scope="col">Folio Venta</th>
          <th scope="col">Clave Cliente</th>
          <th scope="col">Nombre</th>
          <th scope="col">Total</th>
          <th scope="col">Fecha</th>
        </tr>
      </thead>
      <tbody>
        @foreach ($sales as $sale)
            <tr>
              <td>{{$sale->id}}</td>
              <td>{{$sale->customer_id}}</td>
              <td>{{ $sale->customer->full_name }}</td>
              <td>{{$sale->total_owed}}</td>
              <td>{{$sale->created_at}}</td>
            </tr>
           
        @endforeach
      </tbody>
    </table>
  
    {!! $sales->links() !!}
</div>
      
@endsection