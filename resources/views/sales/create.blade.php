@extends('layouts.app')

@section('content')
<div class="container" style="border:solid thin gray;">
   
    <div class="col-sm-12">
        <div class="card" style="color:green;">
 		<div class="card-header bg-primary">
	        <span class="text-white">Registro de Ventas</span>
	     </div>
 		</div>
	</div><br>
	<div class="col-sm-12">
		<p class="card-title text-right" style="margin-right: 50px;">Folio Venta: 
			@if(!empty($sale))
                 {{ $sale->id + 1 }}
              @else
                1
              @endif
	    </p>
	</div>

    <createsales></createsales>       
    
</div>
@endsection