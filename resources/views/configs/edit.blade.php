@extends('layouts.app')

@section('content')
<div class="container">
    
    <div class="card" style="color:green;">

      <div class="card-header bg-primary">
        <span class="text-white">Configuración General</span>
      </div>
        <div class="card-body">

            <form  action="{{ route('configs.update', $config->id) }}" method="POST">
                @csrf <!-- {{ csrf_field() }} -->
                @method('PUT')
              <div class="form-group row text-right">
                <label for="financing_rate" class="col-sm-2 col-form-label">Tasa Financiamiento:</label>
                <div class="col-sm-4">
                  <input type="text" name="financing_rate" class="form-control" id="financing_rate" value="{{ $config->financing_rate }}">
                </div>
              </div>

              <div class="form-group row text-right">
                <label for="hooking_percentage" class="col-sm-2 col-form-label">% Enganche:</label>
                <div class="col-sm-4">
                  <input type="text" name="hooking_percentage" class="form-control" id="hooking_percentage" value="{{ $config->hooking_percentage }}">
                </div>
              </div>

              <div class="form-group row text-right">
                <label for="deadline" class="col-sm-2 col-form-label">Plazo Máximo:</label>
                <div class="col-sm-4">
                  <input type="text" name="deadline" class="form-control" id="deadline" value="{{ $config->deadline }}">
                </div>
              </div>

        

              <div class="form-group row text-right">
                <div class="col-sm-10">
                  <a class="btn btn-success" href="{{ "/home" }}">Cancelar</a>
                  <button type="submit" class="btn btn-success">Guardar</button>
                </div>
              </div>

            </form>
    
        </div>

    </div>  
</div>

@endsection