<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
      rel="stylesheet">
    
    <!-- Favicon -->
    <link rel="shortcut icon" href="{{ asset('img/favicon.png') }}">
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/toastr.css') }}" rel="stylesheet">
</head>
<body>
    <div id="app">
        
        <div class="container-fluid">
            
            <div class="text-right">
                <a class="navbar-brand" href="{{ url('/') }}">
                    
                        <h2 class="text-success"><b>La Vendimia</b></h2>
                    
                </a>
            </div>
        </div>
     

        <div class="container-fluid">
            <nav class="navbar navbar-expand-lg navbar-dark bg-dark navbar-laravel">
              
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

              <div class="collapse navbar-collapse col-sm-7" id="navbarNavDropdown">
                <ul class="navbar-nav">
                 
                  <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      Inicio
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                        <a class="dropdown-item" href="/sales">Ventas</a><hr>
                        <a class="dropdown-item" href="/customers">Clientes</a>
                        <a class="dropdown-item" href="/articles">Artículos</a>
                        <a class="dropdown-item" href="/configs">Configuración</a>
                        <hr>
                        <!-- Authentication Links -->
                        @guest
                           
                            <a class="nav-link" href="{{ route('login') }}">{{ __('Iniciar sesión') }}</a>
                            
                            @if (Route::has('register'))
                                
                                <a class="nav-link" href="{{ route('register') }}">{{ __('Registrar nuevo usuario') }}</a>
                                
                            @endif
                        @else
                           
                                    <a class="dropdown-item " href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Cerrar sesión') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                
                        
                        @endguest
                    </div>
                  </li>
                </ul>


              </div>
              <div class="col-sm-5 text-right text-white">Fecha:  {{ date('d/m/Y') }}</div>

            </nav>

        </div>
        
        <div class="container-fluid">
            @if ($errors->any())
                <div class="alert alert-danger">
                    <strong>No es posible continuar</strong> encontramos detalles en los siguientes campos:<br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            @if ($message = Session::get('success'))
                <div class="alert alert-success">
                    <p>{{ $message }}</p>
                </div>
            @endif
        </div>
        <main class="py-4">
            @yield('content')
        </main>
    </div>
</body>
</html>
