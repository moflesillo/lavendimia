@extends('layouts.app')

@section('content')
<div class="container">
    
    <div class="card" style="color:green;">

      <div class="card-header bg-primary">
        <span class="text-white">Registro de Artículos</span>
      </div>

   
    
        <div class="card-body">
            <p class="card-title text-right" style="margin-right: 50px;">Clave:
              @if(!empty($article))
                 {{ $article->id + 1 }}
              @else
                1
              @endif
            
            </p>


            <form  action="{{ route('articles.store') }}" method="POST">
                @csrf <!-- {{ csrf_field() }} -->
                
              <div class="form-group row text-right">
                <label for="description" class="col-sm-2 col-form-label">Descripción:</label>
                <div class="col-sm-4">
                  <input type="text" name="description" class="form-control" id="description" value="{{ old('description') }}">
                </div>
              </div>

              <div class="form-group row text-right">
                <label for="model" class="col-sm-2 col-form-label">Modelo:</label>
                <div class="col-sm-4">
                  <input type="text" name="model" class="form-control" id="model" value="{{ old('model') }}">
                </div>
              </div>

              <div class="form-group row text-right">
                <label for="price" class="col-sm-2 col-form-label">Precio:</label>
                <div class="col-sm-4">
                  <input type="text" name="price" class="form-control" id="price" value="{{ old('price') }}">
                </div>
              </div>

              <div class="form-group row text-right">
                <label for="existence" class="col-sm-2 col-form-label">Existencia:</label>
                <div class="col-sm-4">
                  <input type="text" name="existence" class="form-control" id="existence" value="{{ old('existence') }}">
                </div>
              </div>

              <div class="form-group row text-right">
                <div class="col-sm-10">
                  <a class="btn btn-success" href="{{ route('articles.index') }}">Cancelar</a>
                  <button type="submit" class="btn btn-success">Guardar</button>
                </div>
              </div>

            </form>
    
        </div>

    </div>  
</div>

@endsection