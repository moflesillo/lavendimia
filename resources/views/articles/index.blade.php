@extends('layouts.app')
 
@section('content')
    
<div class="container">
    


   <div class="row">

       <div class="col-8">
           <h4 class="text-primary" style="position: absolute;
   bottom:0px; top:12px;">Artículos Registrados</h4>
       </div>
       <div class="col text-right">

           <a href="{{ route('articles.create') }}" class="btn btn-primary btn-lg" style="color:white">
            <i style="font-size:40px; color:#38c172; vertical-align:middle; " class="material-icons">add_circle</i> 
              Nuevo Artículo</a>
       </div>
       

   </div>
   <hr>
    <table class="table">
      <thead class="thead-light">
        <tr>
          <th scope="col">Clave Artículo</th>
          <th scope="col">Descripción</th>
          <th width="280px">Acción</th>
        </tr>
      </thead>
      <tbody>
        @foreach ($articles as $article)
            <tr>
              <td>{{$article->id}}</td>
              <td>{{$article->description}}</td>
              <td>
     
                <a title="editar cliente" href="{{ route('articles.edit',$article->id) }}"><i class="material-icons">edit</i>
                </a>

              </td>
            </tr>
           
        @endforeach
      </tbody>
    </table>
  
    {!! $articles->links() !!}
</div>
      
@endsection