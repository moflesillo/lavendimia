@extends('layouts.app')

@section('content')
<div class="container">
    
    <div class="card" style="color:green;">

      <div class="card-header bg-primary">
        <span class="text-white">Editar Artículo</span>
      </div>
    
        <div class="card-body">
            <p class="card-title text-right" style="margin-right: 50px;">Clave: {{$article->id}} </p>


            <form  action="{{ route('articles.update',$article->id) }}" method="POST">
                @csrf <!-- {{ csrf_field() }} -->
                @method('PUT')
              <div class="form-group row text-right">
                <label for="description" class="col-sm-2 col-form-label">Descripción:</label>
                <div class="col-sm-4">
                  <input type="text" name="description" class="form-control" id="description" value="{{ $article->description }}">
                </div>
              </div>

              <div class="form-group row text-right">
                <label for="model" class="col-sm-2 col-form-label">Modelo:</label>
                <div class="col-sm-4">
                  <input type="text" name="model" class="form-control" id="model" value="{{ $article->model }}">
                </div>
              </div>

              <div class="form-group row text-right">
                <label for="mother_lastname" class="col-sm-2 col-form-label">Precio:</label>
                <div class="col-sm-4">
                  <input type="text" name="price" class="form-control" id="price" value="{{ $article->price }}">
                </div>
              </div>

              <div class="form-group row text-right">
                <label for="existence" class="col-sm-2 col-form-label">Existencia:</label>
                <div class="col-sm-4">
                  <input type="text" name="existence" class="form-control" id="existence" value="{{ $article->existence }}">
                </div>
              </div>

              <div class="form-group row text-right">
                <div class="col-sm-10">
                  <a class="btn btn-success" href="{{ route('articles.index') }}">Cancelar</a>
                  <button type="submit" class="btn btn-success">Guardar</button>
                </div>
              </div>

            </form>
    
        </div>

    </div>
</div>

@endsection