@extends('layouts.app')
 
@section('content')
    
<div class="container">
    

    

   <div class="row">

       <div class="col-8">
           <h4 class="text-primary" style="position: absolute;
   bottom:0px; top:12px;">Clientes Registrados</h4>
       </div>
       <div class="col text-right">

           <a href="{{ route('customers.create') }}" class="btn btn-primary btn-lg" style="color:white">
            <i style="font-size:40px; color:#38c172; vertical-align:middle; " class="material-icons">add_circle</i> 
              Nuevo Cliente</a>
       </div>
       

   </div>
   <hr>
    <table class="table">
      <thead class="thead-light">
        <tr>
          <th scope="col">Clave Cliente</th>
          <th scope="col">Nombre</th>
          <th width="280px">Acción</th>
        </tr>
      </thead>
      <tbody>
        @foreach ($customers as $customer)
            <tr>
              <td>{{$customer->id}}</td>
              <td>{{$customer->name}} {{$customer->father_lastname}} {{$customer->mother_lastname}}</td>
              <td>
     
                <a title="editar cliente" href="{{ route('customers.edit',$customer->id) }}"><i class="material-icons">edit</i>
                </a>

              </td>
            </tr>
           
        @endforeach
      </tbody>
    </table>
  
    {!! $customers->links() !!}
</div>
      
@endsection