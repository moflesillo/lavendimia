@extends('layouts.app')

@section('content')
<div class="container">
    
    <div class="card" style="color:green;">

      <div class="card-header bg-primary">
        <span class="text-white">Editar Cliente</span>
      </div>
    
        <div class="card-body">
            <p class="card-title text-right" style="margin-right: 50px;">Clave: {{$customer->id}} </p>


            <form  action="{{ route('customers.update',$customer->id) }}" method="POST">
                @csrf <!-- {{ csrf_field() }} -->
                @method('PUT')
              <div class="form-group row text-right">
                <label for="name" class="col-sm-2 col-form-label">Nombre:</label>
                <div class="col-sm-4">
                  <input type="text" name="name" class="form-control" id="name" value="{{ $customer->name }}">
                </div>
              </div>

              <div class="form-group row text-right">
                <label for="father_lastname" class="col-sm-2 col-form-label">Apellido Paterno:</label>
                <div class="col-sm-4">
                  <input type="text" name="father_lastname" class="form-control" id="father_lastname" value="{{ $customer->father_lastname }}">
                </div>
              </div>

              <div class="form-group row text-right">
                <label for="mother_lastname" class="col-sm-2 col-form-label">Apellido Materno:</label>
                <div class="col-sm-4">
                  <input type="text" name="mother_lastname" class="form-control" id="mother_lastname" value="{{ $customer->mother_lastname }}">
                </div>
              </div>

              <div class="form-group row text-right">
                <label for="rfc" class="col-sm-2 col-form-label">RFC:</label>
                <div class="col-sm-4">
                  <input type="text" name="rfc" class="form-control" id="rfc" value="{{ $customer->rfc }}">
                </div>
              </div>

              <div class="form-group row text-right">
                <div class="col-sm-10">
                  <a class="btn btn-success" href="{{ route('customers.index') }}">Cancelar</a>
                  <button type="submit" class="btn btn-success">Guardar</button>
                </div>
              </div>

            </form>
    
        </div>

    </div>
</div>

@endsection