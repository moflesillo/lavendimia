@extends('layouts.app')

@section('content')
<div class="container">
    
    <div class="card" style="color:green;">

      <div class="card-header bg-primary">
        <span class="text-white">Registro de Clientes</span>
      </div>
    
        <div class="card-body">
            <p class="card-title text-right" style="margin-right: 50px;">Clave: 

              @if(!empty($article))
                 {{ $article->id + 1 }}
              @else
                1
              @endif
            </p> 
            <form  action="{{ route('customers.store') }}" method="POST">
                @csrf <!-- {{ csrf_field() }} -->
                
              <div class="form-group row text-right">
                <label for="name" class="col-sm-2 col-form-label">Nombre:</label>
                <div class="col-sm-4">
                  <input type="text" name="name" class="form-control" id="name" value="{{ old('name') }}">
                </div>
              </div>

              <div class="form-group row text-right">
                <label for="father_lastname" class="col-sm-2 col-form-label">Apellido Paterno:</label>
                <div class="col-sm-4">
                  <input type="text" name="father_lastname" class="form-control" id="father_lastname" value="{{ old('father_lastname') }}">
                </div>
              </div>

              <div class="form-group row text-right">
                <label for="mother_lastname" class="col-sm-2 col-form-label">Apellido Materno:</label>
                <div class="col-sm-4">
                  <input type="text" name="mother_lastname" class="form-control" id="mother_lastname" value="{{ old('mother_lastname') }}">
                </div>
              </div>

              <div class="form-group row text-right">
                <label for="rfc" class="col-sm-2 col-form-label">RFC:</label>
                <div class="col-sm-4">
                  <input type="text" name="rfc" class="form-control" id="rfc" value="{{ old('rfc') }}">
                </div>
              </div>

              <div class="form-group row text-right">
                <div class="col-sm-10">
                  <a class="btn btn-success" href="{{ route('customers.index') }}">Cancelar</a>
                  <button type="submit" class="btn btn-success">Guardar</button>
                </div>
              </div>

            </form>
    
        </div>

    </div>


    {{-- <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Add New Product</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('customers.index') }}"> Back</a>
            </div>
        </div>
    </div> --}}
       
    {{-- @if ($errors->any())
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif --}}
       
    {{-- <form action="{{ route('customers.store') }}" method="POST">
        @csrf
      
         <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Name:</strong>
                    <input type="text" name="name" class="form-control" placeholder="Name">
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Detail:</strong>
                    <textarea class="form-control" style="height:150px" name="detail" placeholder="Detail"></textarea>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                    <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </div>
       
    </form> --}}   
</div>

@endsection