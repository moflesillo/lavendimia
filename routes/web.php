<?php
use Illuminate\Support\Facades\Input as Input;
use App\Customer;
use App\Article;
use App\Config;
use App\Sale;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::resource('customers','CustomerController');
Route::resource('articles','ArticleController');
Route::resource('configs','ConfigController');
Route::resource('sales','SaleController');


Route::get('/getSales', 'SaleController@getSales');

Route::get('/customersget/',function(){
 $query = Input::get('query');
 $customers = Customer::where('name','like','%'.$query.'%')->orWhere('father_lastname','like','%'.$query.'%')->orWhere('mother_lastname','like','%'.$query.'%')->get();
 return response()->json($customers);
});

Route::get('/articlesget',function(){
 $queryart = Input::get('queryart');
 $articles = Article::where('description','like','%'.$queryart.'%')->orWhere('model','like','%'.$queryart.'%')->get();
 return response()->json($articles);
});

Route::get('/getprice',function(){
 $article_id = Input::get('id');
 $config = Config::orderBy('created_at','desc')->first();
 $article = Article::find($article_id);
 //Precio = Precio Articulo X (1 + (Tasa Financiamiento X Plazo Máximo) /100)
 $price = $article->price * (1+($config->financing_rate * $config->deadline) / 100);
 return response()->json(round($price,2));
 // $articles = Article::where('description','like','%'.$queryart.'%')->orWhere('model','like','%'.$queryart.'%')->get();
 // return response()->json($articles);
});

Route::get('/getTotales',function(){
 $sumAmount = Input::get('sumAmount');
 $config = Config::orderBy('created_at','DESC')->first();
 $totales["amount"] = round($sumAmount,2);
 $totales["hooking"] =  round(($config->hooking_percentage/100) * $totales["amount"],2);
 $totales["hooking_bonus"] = round($totales["hooking"] * (($config->financing_rate * $config->deadline) /100),2);
 $totales["total"] =   round($totales["amount"] - $totales["hooking"] - $totales["hooking_bonus"],2);
 return response()->json($totales);
});

Route::get('/getPlazos',function(){
 $sumAmount = Input::get('sumAmount');
 $config = Config::orderBy('created_at','DESC')->first();

 $plazos[3]["precio_contado"] = $sumAmount / (1 + (($config->financing_rate * $config->deadline) / 100));
 $plazos[3]["plazo"] = 3;
 $plazos[3]["total"] = round($plazos[3]["precio_contado"] * (1 + ($config->financing_rate * 3) / 100),2);
 $plazos[3]["abono"] = round($plazos[3]["total"] / 3,2);
 $plazos[3]["ahorro"] = round($sumAmount - $plazos[3]["total"],2);

 $plazos[6]["plazo"] = 6;
 $plazos[6]["total"] =  round($plazos[3]["precio_contado"] * (1 + ($config->financing_rate * 6) / 100),2);
 $plazos[6]["abono"] = round($plazos[6]["total"] / 6,2);
 $plazos[6]["ahorro"] = round($sumAmount - $plazos[6]["total"],2);

 $plazos[9]["plazo"] = 9;
 $plazos[9]["total"] = round($plazos[3]["precio_contado"] * (1 + ($config->financing_rate * 9) / 100),2);
 $plazos[9]["abono"] = round($plazos[9]["total"] / 9,2);
 $plazos[9]["ahorro"] = round($sumAmount - $plazos[9]["total"],2);

 $plazos[12]["plazo"] = 12;
 $plazos[12]["total"] = round($plazos[3]["precio_contado"] * (1 + ($config->financing_rate * 12) / 100),2);
 $plazos[12]["abono"] = round($plazos[12]["total"] / 12,2);
 $plazos[12]["ahorro"] = round($sumAmount - $plazos[12]["total"],2);

 return response()->json($plazos);
});

Route::post('/saveSale',function(){
	//QUITAR EXISTENCIAS DE LOS ARTÍCULOS
	$articles = Input::get('articles');
	foreach ($articles as $art) {

		$article = Article::find($art["id"]);
		$article->existence = $article->existence - $art["quantity"];
		$article->save();
	}

	$config = Config::orderBy('created_at','DESC')->first();
	$sale = new Sale;
	$sale->customer_id = Input::get('customer_id');
	$sale->hooking = Input::get('hooking');
	$sale->hooking_bonus = Input::get('hooking_bonus');
	$sale->total_owed = Input::get('total_owed');
	$sale->monthly_payment_number = Input::get('monthly_payment_number');
	$sale->cash_price = Input::get('total_owed') / (1 + (($config->financing_rate * $config->deadline) / 100));
	$sale->payment_ammount = round( Input::get('total_owed')/ $sale->monthly_payment_number ,2);
 	$sale->monthly_payment_number = Input::get('monthly_payment_number');
 	$sale->save();
 	return response()->json($sale);
});